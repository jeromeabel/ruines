
int relay = 13;
int nb_delay = 5; // 2minutes 30sec

void setup() {
  pinMode(relay, OUTPUT);
  digitalWrite(relay, LOW);
  delay(10000);
}

void loop() {
  digitalWrite(relay, HIGH);
  delay(100);
  digitalWrite(relay, LOW);

  // delai
  for (int i=0 ; i < nb_delay ; i++) {
    delay(30000);
  }
}
