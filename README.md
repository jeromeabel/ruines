# Ruines

Installation artistique sur le sel et ses transformations.


## RPI ZERO
Modèles : rpi zero v1.1 2017

- sd card 16Go, class 10
- install rpi os : 2022-01-28-raspios-bullseye-armhf.img (pas full, pas lite)
- balenaEtcher
- rpi setup (location, wifi, update, enable camera + ssh, changer hostname+pass) : rpizero1 ou rpizero2 / rpi
- updates


## RPI ZERO CAM
- test : raspistill -k ou raspistill -o test.jpg
- Menu Preferences > rpi config > interfaces > Camera enabled ou sudo raspi-config > interfaces

Caractéristiques: 
- Raspberry Pi Camera, supporte Raspberry Pi Zéro W et v1.3
- Capteur OV5647 de 5 mégapixels
- Champ de vision: 72,4 degré
- Résolution du capteur: 1080p
- 2592 × 1944 résolution d'image fixe
- Support des enregistrements vidéo 1080p30, 720p60 et 640x480p60 / 90
- Dimensions: 60mm × 11.5mm × 5mm



## TIME-LAPSE
- mkdir /home/pi/images
- nano /home/pi/time-lapse.py


## MEM
- df -h : reste 10Go
- poids images entre 2.4Mo et 3Mo, on peut mettre 3500 images
- 1 images toutes les heures = 20 images max / j . 3500 / 20 = 175j. 175/6 (jours ouvrables) = 29 semaines possibles
- avril (2smaines=, mai/juin/juillet/aout/septembre/octobre (6 x 4 semaines + 2) = 26 semaines

## BOOT
- sudo nano ~/.config/lxsession/LXDE-pi/autostart

''''
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@xscreensaver -no-splash
@python3 /home/pi/time-lapse.py
''''


## CMD
- 192.168.43.212 : rpizero1 (en bas)
- 192.168.43.179 : rpizero2 (en haut)
- ssh -X pi@192.168.43.212
- ssh -X pi@192.168.43.179
- ls images
- killall python3
- python3 time-lapse.py
- display images/2022-....
- images > video: ffmpeg -framerate 3 -i makingof%02d.jpg output_3fps.mp4
- video for web: ffmpeg -i output_3fps.mp4 -vcodec libvpx -qmin 0 -qmax 50 -crf 10 -b:v 1M -acodec libvorbis output.webm


	
	


