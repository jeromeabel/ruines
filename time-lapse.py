# -*- coding:utf-8 -*-
from picamera import PiCamera
import time
from time import sleep
import os

# get unique counter (number of images)
current = len([f for f in os.listdir('./images')])
current += 1

camera = PiCamera()
camera.resolution = (2592, 1944)

while True:
    variable = "/home/pi/images/" + str(current) + ".jpg"
    camera.capture(variable)
    current += 1
    sleep(3600)
